from celery import Celery
from flask import Flask, request
from datetime import timedelta
from flask_mail import Mail,Message
from flask_celery import make_celery
from celery.schedules import crontab
flask_app = Flask(__name__)
flask_app.config['MAIL_SERVER']='smtp.gmail.com'
flask_app.config['MAIL_PORT']=587
flask_app.config['MAIL_USE_SSL']=False
flask_app.config['MAIL_USE_TLS']=True
flask_app.config['MAIL_USERNAME']='spremjeet3@gmail.com'
flask_app.config['MAIL_PASSWORD']='9140486820'
flask_app.config['CELERY_BROKER_URL']='amqp://localhost//'
flask_app.config['CELERY_RESULT_BACKENED']='amqp://localhost//'
# app = Celery('periodic', broker="pyamqp://guest@localhost//")
celery= make_celery(flask_app)
celery=Celery('periodic',broker=flask_app.config['CELERY_BROKER_URL'])
celery.conf.update(result_backened=flask_app.config['CELERY_RESULT_BACKENED'])
mail=Mail(flask_app)
# @celery_app.task
# def mail_send():
#     print("message sending")
#     # email = request.json.get('email', None)
#     msg = Message("hello", sender='spremjeet3@gmail.com',
#                   recipients=['spremjeet3@gmail.com', 'premjeetsingh762@gmail.com'])
#     mail.send(msg)
#     return "message sent"


@celery.task
def mail_send():
    with flask_app.app_context():
        print("message sending")
        # email = request.json.get('email', None)
        msg = Message("hello", sender='spremjeet3@gmail.com',
                      recipients=['spremjeet3@gmail.com', 'premjeetsingh762@gmail.com'])
        mail.send(msg)
        return "message sent"



celery.conf.beat_schedule = {
    "see-you-in-ten-seconds-task": {
        "task": "periodic.mail_send",
        "schedule": 1
    }
}
# celery.conf.beat_schedule = {
#     # Executes every Monday morning at 7:30 a.m.
#     'add-every-monday-morning': {
#         'task': 'periodic.sees_you',
#         'schedule': crontab(hour=7, minute=30, day_of_week=1),
#         'args': (16, 16),
#     },
# }