_DELIVERY_MODE_PERSISTENT=2
import pika
parameters=pika.URLParameters('amqp://guest:guest@localhost:5672/')
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
email='spremjeet33@gmail.com'

channel.queue_declare(queue='tests_email_queues')
channel.basic_publish('',
                      'tests_email_queues',
                      body=email,
    properties=pika.BasicProperties(
        delivery_mode=_DELIVERY_MODE_PERSISTENT
    )
)
channel.close()

