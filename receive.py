import pika
import requests
from celery import Celery
from flask import Flask, request
from datetime import timedelta
from flask_mail import Mail,Message
from flask_celery import make_celery
from celery.schedules import crontab
flask_app = Flask(__name__)
flask_app.config['MAIL_SERVER']='smtp.gmail.com'
flask_app.config['MAIL_PORT']=587
flask_app.config['MAIL_USE_SSL']=False
flask_app.config['MAIL_USE_TLS']=True
flask_app.config['MAIL_USERNAME']='spremjeet3@gmail.com'
flask_app.config['MAIL_PASSWORD']='9140486820'
mail=Mail(flask_app)


_DELIVERY_MODE_PERSISTENT=2

# Configuration
DOMAIN = 'example.com'
MAILGUN_API_KEY = 'YOUR_MAILGUN_API_KEY'
RABBITMQ_HOST = 'localhost'
RETRY_DELAY_MS = 30000

parameters=pika.URLParameters('amqp://guest:guest@localhost:5672/')
connection = pika.BlockingConnection(parameters)
channel=connection.channel()
channel.queue_declare(queue='tests_email_queues')

retry_channel = connection.channel()
retry_channel.queue_declare(
    queue='retry_tests_email_queues',
    durable=True,
    arguments={
        'x-message-ttl': RETRY_DELAY_MS,
        'x-dead-letter-exchange': 'amq.direct',
        'x-dead-letter-routing-key': 'tests_email_queues'
    }
)
def send_message(ch, method, properties,body):
    # import pdb
    # pdb.set_trace();
    email='spremjeet3@gmail.com'
    print(body)
    # address = body.decode('UTF-8')
    # print("Sending welcome email to {}".format(address))
    # res = requests.post(
    #     "https://api.mailgun.net/v3/{}/messages".format(DOMAIN),
    #     auth=("api", MAILGUN_API_KEY),
    #     data={"from": "Flaskr <noreply@{}>".format(DOMAIN),
    #           "to": [address],
    #           "subject": "Welcome to Flaskr!",
    #           "text": "Welcome to Flaskr, your account is now active!"}
    # )
    try:
        with flask_app.app_context():
            msg = Message("hello", sender='spremjeet3@gmail.com',
                          recipients=['spremjeet3@gmail.com', 'premjeetsingh762@gmail.com'])
            mail.send(msg)
        # ch.basic_ack(delivery_tag=method.delivery_tag)
    except:
        retry_channel.basic_publish(
            exchange='',
            routing_key='retry_tests_email_queues',
            body=email,
            properties=pika.BasicProperties(
                delivery_mode=_DELIVERY_MODE_PERSISTENT
            )
        )
    # retry_channel.basic_publish(
    #         exchange='',
    #         routing_key='retry_queue',
    #         body=email,
    #         properties=pika.BasicProperties(
    #             delivery_mode=_DELIVERY_MODE_PERSISTENT
    #         )
    #     )


channel.basic_consume(queue='tests_email_queues',on_message_callback=send_message,auto_ack=True)
channel.start_consuming()